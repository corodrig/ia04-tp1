// Created by Côme Rodriguez and Luning Yang

package server

import (
	"log"
	"net/http"
	"time"
)

// NewManagerAgent creates a new ManagerAgent responsible of
// managing all the ballots (give the id and store all the ballots availaible
// on the server)
func NewManagerAgent(addr string) *ManagerAgent {
	b_agts := make([]BallotAgent, 0)
	return &ManagerAgent{Addr: addr, Ballot_current_id: 0, Ballots: b_agts}
}

// Start is a function applicable on a ManagerAgent and starts a server (i.e set available
// all its functionalities to the clients) to let clients create ballot and vote.
func (manager *ManagerAgent) Start() {
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", manager.NewBallot)
	mux.HandleFunc("/vote", manager.NewVote)
	mux.HandleFunc("/result", manager.GetResult)

	s := &http.Server{
		Addr:           manager.Addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	log.Println("Listening on", manager.Addr)
	log.Fatal(s.ListenAndServe())
}
