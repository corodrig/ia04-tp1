// Created by Côme Rodriguez and Luning Yang

package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04/agt/client"
	"ia04/comsoc"
	"log"
	"net/http"
	"time"
)

// NewVoterAgent creates a new VoterAgent and returns its adress.
func (manager *ManagerAgent) NewVoterAgent(id string, prefs []comsoc.Alternative) *VoterAgent {
	return &VoterAgent{ID: AgentID(id), Prefs: prefs}
}

// decodeNewVoteRequest decodes a request for creating a new vote sent by a client
// on the server.
func (manager *ManagerAgent) decodeNewVoteRequest(r *http.Request) (req agt.VoteRequest, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

// NewVote creates a new vote (i.e a VoterAgent votes in a ballot) in the server.
func (manager *ManagerAgent) NewVote(w http.ResponseWriter, r *http.Request) {
	manager.Lock()
	defer manager.Unlock()
	log.Println("NEW VOTE: Received request of new vote")
	req, err := manager.decodeNewVoteRequest(r)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		log.Println("NEW VOTE: Error encountered during decoding of the new vote request.")
		return
	}
	var have_voted bool = false
	for _, ballot := range manager.Ballots {
		if ballot.ID == BallotID(req.VoteID) {
			if ballot.Deadline.Sub(time.Now()) < 0 {
				w.WriteHeader(http.StatusServiceUnavailable)
				log.Println("Can't vote: deadline is passed")
				return
			}
			for i := range ballot.Voters {
				if ballot.Voters[i].ID == AgentID(req.AgentID) && len(ballot.Voters[i].Prefs) == 0 {
					ballot.Voters[i].Prefs = req.Prefs
					ballot.Voters[i].Options = req.Options
					w.WriteHeader(http.StatusOK)
					log.Printf("Agent %s have voted for ballot %s", ballot.Voters[i].ID, ballot.ID)
					fmt.Println("BALLOT VOTES ", ballot.Voters)
					have_voted = true
					return
				} else if ballot.Voters[i].ID == AgentID(req.AgentID) && len(ballot.Voters[i].Prefs) > 0 {
					w.WriteHeader(http.StatusForbidden)
					log.Println("Agent already voted for this ballot")
					return
				}
			}
		}
	}
	if !have_voted {
		w.WriteHeader(http.StatusNotImplemented)
		log.Println("This agent is not registered for this ballot or the ballot does not exist")
		return
	}
}
