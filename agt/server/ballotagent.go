// Created by Côme Rodriguez and Luning Yang

package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	agt "ia04/agt/client"
	"ia04/comsoc"
	"log"
	"net/http"
	"time"
)

// NewBallotAgent creates a new BallotAgent and returns its adress.
func (manager *ManagerAgent) NewBallotAgent(rule string, deadline time.Time, voters_ids []string) *BallotAgent {
	id := fmt.Sprintf("vote%d", manager.Ballot_current_id)
	manager.Ballot_current_id += 1

	voters := make([]VoterAgent, 0)
	for _, voter := range voters_ids {
		voters = append(voters, *manager.NewVoterAgent(voter, []comsoc.Alternative{}))
	}
	return &BallotAgent{ID: BallotID(id), Rule: rule, Deadline: deadline, Voters: voters}
}

// decodeNewBallotRequest decodes a request for creating a new BallotAgent sent by a client
// on the server.
func (manager *ManagerAgent) decodeNewBallotRequest(r *http.Request) (req agt.NewBallotRequest, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

// NewBallot creates a new ballot in the server and returns some informations if
// the creation is a sucess.
func (manager *ManagerAgent) NewBallot(w http.ResponseWriter, r *http.Request) {
	manager.Lock()
	defer manager.Unlock()

	log.Println("NEW BALLOT: Received request of new ballot")

	req, err := manager.decodeNewBallotRequest(r)
	var resp agt.NewBallotReturned
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		log.Println("NEW BALLOT: Error encountered during decoding of the new ballot request.")
		return
	}
	if req.Rule != "approval" && req.Rule != "borda" && req.Rule != "copeland" && req.Rule != "kemeny" && req.Rule != "majority" && req.Rule != "stv" {
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
	deadline, er := time.Parse("Jan 2, 2006 at 3:04pm", req.Deadline)
	if er != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	new_ballot := manager.NewBallotAgent(req.Rule, deadline, req.Voters_ids)
	new_ballot.Alts = make([]comsoc.Alternative, req.Alts)
	manager.Ballots = append(manager.Ballots, *new_ballot)
	for i := 0; i < req.Alts; i++ {
		new_ballot.Alts[i] = comsoc.Alternative(i + 1)
	}

	resp.BallotID = string(new_ballot.ID)
	log.Printf("NEW BALLOT: Ballot '%s' created.", resp.BallotID)
	w.WriteHeader(http.StatusCreated)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

// decodeResultsRequest decodes a request for getting the result of a ballot sent by a client
// on the server.
func (manager *ManagerAgent) decodeResultsRequest(r *http.Request) (req agt.ResultRequest, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

// GetResult returns the result of a ballot if it's possible (i.e if the deadline is passed)
func (manager *ManagerAgent) GetResult(w http.ResponseWriter, r *http.Request) {
	manager.Lock()
	defer manager.Unlock()

	log.Println("Request for results of a vote received")
	req, err := manager.decodeResultsRequest(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		log.Println("NEW BALLOT: Error encountered during decoding of the new ballot request.")
		return
	}

	for _, ballot := range manager.Ballots {
		if ballot.ID == BallotID(req.BallotID) {
			if ballot.Deadline.Sub(time.Now()) < 0 {
				profile := make(comsoc.Profile, 0)
				for i := 0; i < len(ballot.Voters); i++ {
					if len(ballot.Voters[i].Prefs) > 0 {
						if comsoc.CheckProfileAlternative([][]comsoc.Alternative{ballot.Voters[i].Prefs}, ballot.Alts) == nil {
							profile = append(profile, ballot.Voters[i].Prefs)
						}
					}
				}
				if len(profile) == 0 {
					w.WriteHeader(http.StatusBadRequest)
					log.Println("No valid vote for this ballot")
					return
				}
				var resp agt.ResultReturned
				var win comsoc.Alternative
				ranking := make([]comsoc.Alternative, len(ballot.Alts))
				switch ballot.Rule {
				case "approval":
					thresholds := make([]int, 0)
					for j := 0; j < len(ballot.Voters); j++ {
						thresholds = append(thresholds, ballot.Voters[j].Options[0])
					}
					tie := comsoc.TieBreakFactory(ballot.Alts)
					count, _ := comsoc.ApprovalSWF(profile, thresholds, ballot.Alts)
					ranking = comsoc.TransformCountToSlice(count, tie)
					winner, _ := comsoc.ApprovalSCF(profile, thresholds, ballot.Alts)
					win = comsoc.GetBestAlt(winner, tie)

				case "borda":
					tie := comsoc.TieBreakFactory(ballot.Alts)
					swf := comsoc.SWFFactory(profile, ballot.Alts, comsoc.BordaSWF, tie)
					scf := comsoc.SCFFactory(profile, ballot.Alts, comsoc.BordaSCF, tie)
					ranking, _ = swf(profile)
					win, _ = scf(profile)
				case "copeland":
					tie := comsoc.TieBreakFactory(ballot.Alts)
					swf := comsoc.SWFFactory(profile, ballot.Alts, comsoc.CopelandSWF, tie)
					scf := comsoc.SCFFactory(profile, ballot.Alts, comsoc.CopelandSCF, tie)
					ranking, _ = swf(profile)
					win, _ = scf(profile)
				case "kemeny":
					ranking = comsoc.KemenySWF(profile, ballot.Alts)
					win = comsoc.KemenySCF(profile, ballot.Alts)[0]
				case "majority":
					tie := comsoc.TieBreakFactory(ballot.Alts)
					swf := comsoc.SWFFactory(profile, ballot.Alts, comsoc.MajoritySWF, tie)
					scf := comsoc.SCFFactory(profile, ballot.Alts, comsoc.MajoritySCF, tie)
					ranking, _ = swf(profile)
					win, _ = scf(profile)
				case "stv":
					tie := comsoc.TieBreakFactory(ballot.Alts)
					rank, _ := comsoc.STV_SWF(profile, ballot.Alts, ballot.Alts)
					ranking = comsoc.TransformCountToSlice(rank, tie)
					win = comsoc.STV_SCF(profile, ballot.Alts, ballot.Alts)[0]
				}
				w.WriteHeader(http.StatusOK)
				resp.Winner = win
				resp.Ranking = ranking
				serial, _ := json.Marshal(resp)
				w.Write(serial)
			} else {
				w.WriteHeader(http.StatusTooEarly)
				log.Printf("Error: too early to have the results. Please wait until %s", ballot.Deadline)
				return
			}
		}
	}
}
