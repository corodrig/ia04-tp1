// Created by Côme Rodriguez and Luning Yang
// This file contains usefull types for the server side

package server

import (
	"ia04/comsoc"
	"sync"
	"time"
)

type BallotID string

type AgentID string

type VoterAgent struct {
	ID      AgentID
	Prefs   []comsoc.Alternative
	Options []int
}

type BallotAgent struct {
	ID       BallotID
	Rule     string
	Deadline time.Time
	Voters   []VoterAgent
	Alts     []comsoc.Alternative
}

type ManagerAgent struct {
	sync.Mutex
	Addr              string
	Ballot_current_id int
	Ballots           []BallotAgent
}
