// Created by Côme Rodriguez and Luning Yang
// This file contains usefull types for the client side in
// order to send http request to the server developed in this TP

package agt

import "ia04/comsoc"

type VoteRequest struct {
	AgentID string               `json:"agent-id"`
	VoteID  string               `json:"vote-id"`
	Prefs   []comsoc.Alternative `json:"prefs"`
	Options []int                `json:"options"`
}

type NewBallotRequest struct {
	Rule       string   `json:"rule"`
	Deadline   string   `json:"deadline"`
	Voters_ids []string `json:"voter-ids"`
	Alts       int      `json:"#alts"`
}

type ResultRequest struct {
	BallotID string `json:"ballot-id"`
}

type NewBallotReturned struct {
	BallotID string `json:"ballot-id"`
}

type ResultReturned struct {
	Winner  comsoc.Alternative   `json:"winner"`
	Ranking []comsoc.Alternative `json:"ranking"`
}
