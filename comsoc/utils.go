// Created by Côme Rodriguez and Luning Yang

package comsoc

import (
	"errors"
	"fmt"
	"reflect"
	"sort"
)

// copyProfile copies a profile into another profile
func copyProfile(p Profile) Profile {
	profile_copy := make(Profile, len(p))
	for i, pref := range p {
		profile_copy[i] = pref
	}
	return profile_copy
}

// contains returns true if the alternative alt is in the slice of alternatives alts
func contains(alts []Alternative, alt Alternative) bool {
	for _, v := range alts {
		if v == alt {
			return true
		}
	}
	return false
}

// remove_alt removes an alternative in a slice of alternatives.
func remove_alt(alts []Alternative, i int) []Alternative {
	alts_used := make([]Alternative, len(alts))
	for j := 0; j < len(alts); j++ {
		alts_used[j] = alts[j]
	}
	alts_used = append(alts_used[:i], alts_used[i+1:]...)
	return alts_used
}

// intToAlt converts a slice of int into a slice of Alternative
func intToAlt(a []int) []Alternative {
	alts := make([]Alternative, len(a))
	for i, v := range a {
		alts[i] = Alternative(v)
	}
	return alts
}

// removeAltinProfile removes an alternative alt_to_delete
// in all the preferences of a profile p
func removeAltinProfile(alt_to_delete Alternative, p Profile) {
	for i := 0; i < len(p); i++ {
		var ind int
		for j, alt := range p[i] {
			if alt == alt_to_delete {
				ind = j
			}
		}
		p[i] = remove_alt(p[i], ind)
	}
}

// minCount returns the minimum value (i.e number of vote for an alternative) in a Count
func minCount(c Count) int {
	n_vote := make([]int, 0)
	for _, v := range c {
		n_vote = append(n_vote, v)
	}
	m := n_vote[0]
	for i := 1; i < len(n_vote); i++ {
		if n_vote[i] < m {
			m = n_vote[i]
		}
	}
	return m
}

// getAltsMin returns the alternative(s) having the minimum number of votes
func getAltsMin(c Count, min int) []Alternative {
	alts := make([]Alternative, 0)
	for k, v := range c {
		if v == min {
			alts = append(alts, k)
		}
	}
	return alts
}

// remove-struct removes a key value structure in a slice of key value structures.
func remove_struct(ss []kv, i int) []kv {
	struct_used := make([]kv, len(ss))
	for j := 0; j < len(ss); j++ {
		struct_used[j] = ss[j]
	}
	struct_used = append(struct_used[:i], struct_used[i+1:]...)
	return struct_used
}

// removeDuplicate removes duplicate in a slice of alternatives
func removeDuplicate(altSlice []Alternative) []Alternative {
	allKeys := make(map[Alternative]bool)
	list := []Alternative{}
	for _, item := range altSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

// rank returns the rank of an alternative into preferences
// by a slice of alternatives
func rank(alt Alternative, prefs []Alternative) int {
	var rank int = 0
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt {
			rank = i
		}
	}
	return rank
}

// isPref indicates if the first alternative is prefered
// to the second alternative in a given preference
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	if rank(alt1, prefs) < rank(alt2, prefs) {
		return true
	} else {
		return false
	}
}

// maxCount returns the best alternatives in given vote results
// represented by a map between alternatives and number of votes
func maxCount(count Count) (bestAlts []Alternative) {
	bestAlts = make([]Alternative, 0)
	var ss []kv
	for k, v := range count {
		ss = append(ss, kv{k, v})
	}
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].value > ss[j].value
	})
	bestAlts = append(bestAlts, ss[0].key)
	for i := 1; i < len(ss); i++ {
		if ss[i].value == ss[0].value {
			bestAlts = append(bestAlts, ss[i].key)
		}
	}
	return
}

// checkProfile checks if the profile is valid (i.e all votants have the same
// number of votes and an alternative appear at most one time)
func checkProfile(prefs Profile) error {
	var l int = len(prefs[0])
	for i := 1; i < len(prefs); i++ {
		if len(prefs[i]) != l {
			err := ("Number of votes are not the same for all votants")
			return errors.New(err)
		}
	}
	for i := 0; i < len(prefs); i++ {
		l1 := len(prefs[i])
		l2 := len(removeDuplicate(prefs[i][:]))
		if l1 != l2 {
			err := fmt.Sprintf("An alternative appears more than one time for profile %d", i)
			return errors.New(err)
		}
	}
	return nil
}

// CheckProfileAlternative checks if the profile is valid (i.e if the number of votes
// corresponds to the number of alternative, if all preferences contain all the alternatives
// and if the result of checkProfile is not an error
func CheckProfileAlternative(prefs Profile, alts []Alternative) error {
	basic_error := checkProfile(prefs)
	if basic_error != nil {
		return basic_error
	}
	var l int = len(alts)
	for i := 0; i < len(prefs); i++ {
		if len(prefs[i]) != l {
			err := fmt.Sprintf("Number of votes doesn't match with number of alternatives for profile %d", i)
			return errors.New(err)
		}
		for _, alt := range alts {
			if !contains(prefs[i], alt) {
				err := fmt.Sprintf("Alternative %d not in preferences for profile %d", alt, i)
				return errors.New(err)
			}
		}
	}
	return nil
}

// TieBreak determines the best alternative in a list of alternatives
// according to the following order: an alternative A is prefered to
// an alternative B if A < B
func TieBreak(alts []Alternative) (alt Alternative, err error) {
	alt = alts[0]
	for i := 1; i < len(alts); i++ {
		if alts[i] < alt {
			alt = alts[i]
		}

	}
	err = nil
	return alt, err
}

// Distance computes the Kendall T distance between 2 rankings
func Distance(pref1 []Alternative, pref2 []Alternative, alts []Alternative) int {
	n_diff := 0
	for i := 0; i < len(alts); i++ {
		for j := i; j < len(alts); j++ {
			if alts[i] != alts[j] {
				ispref1 := rank(alts[i], pref1) < rank(alts[j], pref1)
				ispref2 := rank(alts[i], pref2) < rank(alts[j], pref2)
				if ispref1 != ispref2 {
					n_diff += 1
				}
			}
		}
	}
	return n_diff
}

// ProfileDistance computes the distance between a ranking and a profile
// (list of rankings) as the sum of the Distance between the ranking and
// each ranking of the profile
func ProfileDistance(p []Alternative, profile Profile, alts []Alternative) int {
	total_distance := 0
	for _, pref := range profile {
		total_distance += Distance(p, pref, alts)
	}
	return total_distance
}

// TransformCountToSlice is used as the SWFFactory function: the goal is to return
// a sorted slice of Alternatives according to the result of a social welfare function
// and a given tiebreak function
func TransformCountToSlice(c Count, tie func([]Alternative) (alt Alternative, err error)) []Alternative {
	var ss []kv
	for k, v := range c {
		ss = append(ss, kv{k, v})
	}
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].value > ss[j].value
	})
	ss_used := ss
	prev_alternatives := make([]Alternative, 0)
	sorted_alts := make([]Alternative, 0)
	for i := 0; i < len(ss); i++ {
		prev_alternatives = append(prev_alternatives, ss[i].key)
	}
	for {
		new_sorted_alts := make([]Alternative, 0)
		sorted_ss := make([]kv, 0)
		alternatives := prev_alternatives
		for j := 0; j < len(ss)-1; j++ {
			i := 0
			var value_to_add Alternative
			var ss_to_add kv
			if ss_used[i].value == ss_used[i+1].value {
				alt_to_compare := [2]Alternative{ss_used[i].key, ss_used[i+1].key}
				value_to_add, _ = tie(alt_to_compare[:])
				if value_to_add == ss_used[i].key {
					alternatives = remove_alt(alternatives, i)
					ss_to_add = ss_used[i]
					ss_used = remove_struct(ss_used, i)
				} else {
					alternatives = remove_alt(alternatives, i+1)
					ss_to_add = ss_used[i+1]
					ss_used = remove_struct(ss_used, i+1)
				}
			} else {
				value_to_add = ss_used[i].key
				alternatives = remove_alt(alternatives, i)
				ss_to_add = ss_used[i]
				ss_used = remove_struct(ss_used, i)
			}
			new_sorted_alts = append(new_sorted_alts, value_to_add)
			sorted_ss = append(sorted_ss, ss_to_add)
		}
		new_sorted_alts = append(new_sorted_alts, alternatives[0])
		sorted_ss = append(sorted_ss, ss_used[0])
		if reflect.DeepEqual(new_sorted_alts, prev_alternatives) {
			sorted_alts = new_sorted_alts
			break
		}
		prev_alternatives = new_sorted_alts
		ss_used = sorted_ss
	}
	return sorted_alts
}

// GetBestAlt returns the best alternative in a slice according to a tie function
func GetBestAlt(alts []Alternative, tie func([]Alternative) (alt Alternative, err error)) Alternative {
	best_alt := alts[0]
	if len(alts) > 1 {
		best_alt, _ = tie(alts)
	}
	return best_alt
}

// augmentSliceValue adds a value v to each element in a slice of int
func augmentSliceValue(s []int, v int) []int {
	new_s := make([]int, 0)
	for i := 0; i < len(s); i++ {
		new_s = append(new_s, s[i]+v)
	}
	return new_s
}
