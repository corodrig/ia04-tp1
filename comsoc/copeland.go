// Created by Côme Rodriguez and Luning Yang

package comsoc

// CopelandSWF implements the Copeland voting rule as social welfare function
func CopelandSWF(p Profile, alts []Alternative) (Count, error) {
	err := CheckProfileAlternative(p, alts)
	count := make(Count, 0)
	for _, alt := range alts {
		count[alt] = 0
	}
	for i := 0; i < len(alts); i++ {
		for j := i; j < len(alts); j++ {
			if alts[i] != alts[j] {
				condorcet_winner := CondorcetWinner(p, []Alternative{alts[i], alts[j]})
				if len(condorcet_winner) > 0 {
					if condorcet_winner[0] == alts[i] {
						count[alts[i]] += 1
						count[alts[j]] -= 1
					} else {
						count[alts[j]] += 1
						count[alts[i]] -= 1
					}
				}
			}
		}

	}
	return count, err
}

// CopelandSCF implements the Copeland voting rule as social choice function
func CopelandSCF(p Profile, alts []Alternative) ([]Alternative, error) {
	count, err := CopelandSWF(p, alts)
	best_alt := maxCount(count)
	return best_alt, err
}
