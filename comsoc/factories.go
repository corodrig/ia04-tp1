// Created by Côme Rodriguez and Luning Yang

package comsoc

import (
	"reflect"
	"sort"
)

// TieBreakFactory creates a function who returns the best alternative
// in a list of alternatives having the same result according to a given
// order as argument of TieBreakFactory
func TieBreakFactory(order []Alternative) func([]Alternative) (alt Alternative, err error) {
	return func(alts []Alternative) (alt Alternative, err error) {
		alt = alts[0]
		for i := 1; i < len(alts); i++ {
			if rank(alts[i], order) < rank(alt, order) {
				alt = alts[i]
			}
		}
		err = nil
		return alt, err
	}
}

// SWFFactory returns a function having the goal of sorting the alternatives according
// to the result of a social welfare function given as argument swf. The created function
// returns a sorted slice of Alternative according to the result of swf and a tie break if
// there are some ties
func SWFFactory(p Profile, alts []Alternative, swf func(p Profile, alts []Alternative) (Count, error), tie func([]Alternative) (alt Alternative, err error)) func(Profile) ([]Alternative, error) {
	c_swffac, err := swf(p, alts)
	var ss []kv
	for k, v := range c_swffac {
		ss = append(ss, kv{k, v})
	}
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].value > ss[j].value
	})
	return func(p Profile) ([]Alternative, error) {
		ss_used := ss
		prev_alternatives := make([]Alternative, 0)
		sorted_alts := make([]Alternative, 0)
		for i := 0; i < len(ss); i++ {
			prev_alternatives = append(prev_alternatives, ss[i].key)
		}
		for {
			new_sorted_alts := make([]Alternative, 0)
			sorted_ss := make([]kv, 0)
			alternatives := prev_alternatives
			for j := 0; j < len(ss)-1; j++ {
				i := 0
				var value_to_add Alternative
				var ss_to_add kv
				if ss_used[i].value == ss_used[i+1].value {
					alt_to_compare := [2]Alternative{ss_used[i].key, ss_used[i+1].key}
					value_to_add, _ = tie(alt_to_compare[:])
					if value_to_add == ss_used[i].key {
						alternatives = remove_alt(alternatives, i)
						ss_to_add = ss_used[i]
						ss_used = remove_struct(ss_used, i)
					} else {
						alternatives = remove_alt(alternatives, i+1)
						ss_to_add = ss_used[i+1]
						ss_used = remove_struct(ss_used, i+1)
					}
				} else {
					value_to_add = ss_used[i].key
					alternatives = remove_alt(alternatives, i)
					ss_to_add = ss_used[i]
					ss_used = remove_struct(ss_used, i)
				}
				new_sorted_alts = append(new_sorted_alts, value_to_add)
				sorted_ss = append(sorted_ss, ss_to_add)
			}
			new_sorted_alts = append(new_sorted_alts, alternatives[0])
			sorted_ss = append(sorted_ss, ss_used[0])
			if reflect.DeepEqual(new_sorted_alts, prev_alternatives) {
				sorted_alts = new_sorted_alts
				break
			}
			prev_alternatives = new_sorted_alts
			ss_used = sorted_ss
		}
		return sorted_alts, err
	}
}

// SCFFactory returns a function having the goal of returning the best alternative according
// to the result of a social choice function given as argument scf. The created function
// returns a slice of 1 Alternative according to the result of scf and a tie break if
// there is a tie between the results of the scf is composed by more than 1 alternative
func SCFFactory(p Profile, alts []Alternative, scf func(p Profile, alts []Alternative) ([]Alternative, error), tie func([]Alternative) (alt Alternative, err error)) func(Profile) (Alternative, error) {
	best_alts, err := scf(p, alts)
	return func(p Profile) (Alternative, error) {
		best_alt := best_alts[0]
		if len(best_alts) > 1 {
			best_alt, _ = tie(best_alts)
		}
		return best_alt, err
	}
}
