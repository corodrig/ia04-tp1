// Created by Côme Rodriguez and Luning Yang

package comsoc

// ApprovalSWF implements the approval voting rule as social welfare function
func ApprovalSWF(p Profile, thresholds []int, alts []Alternative) (count Count, err error) {
	err = CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(map[Alternative]int)
	for _, alt := range alts {
		count[alt] = 0
		for i, pref := range p {
			if rank(alt, pref)+1 <= thresholds[i] {
				count[alt] += 1
			}
		}
	}
	return count, nil
}

// ApprovalSCF implements the approval voting rule as social choice function
func ApprovalSCF(p Profile, thresholds []int, alts []Alternative) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds, alts)
	bestAlts = maxCount(count)
	return bestAlts, err
}
