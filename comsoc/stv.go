// Created by Côme Rodriguez and Luning Yang

package comsoc

import "fmt"

// STV_SWF implements the simple transferable voting rule as social welfare function
func STV_SWF(p Profile, a []Alternative, order []Alternative) (Count, error) {
	profile_used := copyProfile(p)
	n_a := len(a)
	final_count := make(Count, len(a))
	var e error
	for i := 0; i < n_a-1; i++ {
		new_count, err := MajoritySWF(profile_used, a)
		if err == nil {
			min := minCount(new_count)
			a_min := getAltsMin(new_count, min)
			var alt_to_delete Alternative
			if len(a_min) > 1 {
				alt_to_delete = a_min[0]
				for i := 1; i < len(a_min); i++ {
					if rank(a_min[i], order) > rank(alt_to_delete, order) {
						alt_to_delete = a_min[i]
					}
				}
			} else {
				alt_to_delete = a_min[0]
			}
			var index int
			for i, alt := range a {
				if alt == alt_to_delete {
					index = i
				}
			}
			final_count[alt_to_delete] = i + 1
			delete(new_count, alt_to_delete)
			a = remove_alt(a, index)
			removeAltinProfile(alt_to_delete, profile_used)
		} else {
			e = err
		}
	}
	final_count[a[0]] = n_a
	return final_count, e
}

// STV_SCF implements the simple transferable voting rule as social choice function
func STV_SCF(p Profile, alts []Alternative, order []Alternative) []Alternative {
	count, err := STV_SWF(p, alts, order)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	best_alts := maxCount(count)
	return best_alts
}
