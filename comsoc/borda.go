// Created by Côme Rodriguez and Luning Yang

package comsoc

// BordaSWF implements the Borda voting rule as social welfare function
func BordaSWF(p Profile, alts []Alternative) (Count, error) {
	err := CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count := make(map[Alternative]int)
	for _, alt := range alts {
		count[alt] = 0
		for _, pref := range p {
			count[alt] += len(alts) - 1 - rank(alt, pref)
		}
	}
	return count, nil
}

// BordaSCF implements the Borda voting rule as social choice function
func BordaSCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p, alts)
	bestAlts = maxCount(count)
	return bestAlts, err
}
