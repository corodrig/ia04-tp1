// Created by Côme Rodriguez and Luning Yang

package comsoc

// MajoritySWF implements the majority voting rule as social welfare function
func MajoritySWF(p Profile, alts []Alternative) (count Count, err error) {
	err = CheckProfileAlternative(p, alts)
	if err != nil {
		return nil, err
	}
	count = make(map[Alternative]int)
	for _, alt := range alts {
		count[alt] = 0
		for _, pref := range p {
			if rank(alt, pref) == 0 {
				count[alt] += 1
			}
		}
	}
	return count, nil
}

// MajoritySCF implements the majority voting rule as social choice function
func MajoritySCF(p Profile, alts []Alternative) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p, alts)
	bestAlts = maxCount(count)
	return bestAlts, err
}
