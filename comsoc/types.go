// Created by Côme Rodriguez and Luning Yang

package comsoc

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int
type kv struct {
	key   Alternative
	value int
}
