// Created by Côme Rodriguez and Luning Yang

package comsoc

// CondorcetWinner determines the condorcet winner in a profile of votes and returns it.
// If there is no condorcet winner, it returns an empty slice
func CondorcetWinner(prefs Profile, alts []Alternative) []Alternative {
	winner := make([]Alternative, 0)
	type n_won struct {
		alt   Alternative
		n_won int
	}
	comparison := make([]n_won, 0)
	for i := 0; i < len(alts); i++ {
		comparison = append(comparison, n_won{alts[i], 0})
	}
	for index, alt := range alts {
		for i := 1; i < len(alts)-index; i++ {
			n_vs_win := 0
			for _, pref := range prefs {
				if rank(alt, pref) < rank(alts[i+index], pref) {
					n_vs_win += 1
				}
			}
			if n_vs_win > len(prefs)/2 {
				comparison[index].n_won += 1
			} else {
				comparison[i+index].n_won += 1
			}
		}
		if comparison[index].n_won == len(alts)-1 {
			winner = append(winner, comparison[index].alt)
		}
	}
	return winner
}
