// Created by Côme Rodriguez and Luning Yang

package comsoc

import (
	"gitlab.utc.fr/lagruesy/ia04/utils"
)

// KemenySWF implements the Kemeny voting rule as social welfare function
func KemenySWF(p Profile, alts []Alternative) []Alternative {
	curr_perm := utils.FirstPermutation(len(alts))
	curr_perm = augmentSliceValue(curr_perm, 1)
	perm_alt := intToAlt(curr_perm)
	best_permutation := perm_alt
	for {
		next_perm, ok := utils.NextPermutation(curr_perm)
		next_perm = augmentSliceValue(next_perm, 1)
		if ok {
			next_perm_alt := intToAlt(next_perm)
			if ProfileDistance(next_perm_alt, p, alts) < ProfileDistance(best_permutation, p, alts) {
				best_permutation = next_perm_alt
			}
			curr_perm = next_perm
		} else {
			break
		}
	}
	return best_permutation
}

// KemenySCF implements the Kemeny voting rule as social choice function
func KemenySCF(p Profile, alts []Alternative) []Alternative {
	best_permutation := KemenySWF(p, alts)

	return best_permutation[:1]
}
