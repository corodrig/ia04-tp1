// Created by Côme Rodriguez and Luning Yang

package main

import (
	"fmt"
	rs "ia04/agt/server"
)

// LaunchManagerAgent launches a new Manager Agent (a new server) on the port given in argument
// as a string (for example ":8000").
func LaunchManagerAgent(port string) {
	manager_agent := rs.NewManagerAgent(port)
	manager_agent.Start()
	fmt.Scanln()
}

func main() {
	LaunchManagerAgent(":8080")
}
