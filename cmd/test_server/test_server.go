// Created by Côme Rodriguez and Luning Yang

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	client "ia04/agt/client"
	"ia04/comsoc"
	"net/http"
	"time"
)

// createBallotAndVote is an example of how to use the server in the client side.
// in this function we create a ballot agent and 4 voter agents. The 3 first voter agents
// will express their preferences throught the vote request.
// If you want to test, don't forget to change the deadline. The time is in UTC, for France,
// do hour - 1 ;). You can change the voting rule too.
func createBallotAndVote() {
	fmt.Println("--------------------- BALLOT CREATION ---------------------")
	new_bal := client.NewBallotRequest{Rule: "majority", Deadline: "Nov 7, 2022 at 11:07am", Voters_ids: []string{"agt1", "agt2", "agt3", "agt4"}, Alts: 3}
	data, _ := json.Marshal(new_bal)
	url := "http://127.0.0.1:8080/new_ballot"
	req, _ := http.Post(url, "application/json", bytes.NewBuffer(data))
	buf := new(bytes.Buffer)
	buf.ReadFrom(req.Body)
	resp := client.NewBallotReturned{}
	_ = json.Unmarshal(buf.Bytes(), &resp)
	fmt.Println("CODE ", req.Status)
	fmt.Println("RESP ", resp)
	fmt.Println("-----------------------------------------------------------")
	fmt.Println()

	fmt.Println("-------------------------- VOTE ---------------------------")
	new_vote := client.VoteRequest{AgentID: "agt1", VoteID: "vote0", Prefs: []comsoc.Alternative{1, 2, 3}}
	vote, _ := json.Marshal(new_vote)
	url2 := "http://127.0.0.1:8080/vote"
	req2, _ := http.Post(url2, "application/json", bytes.NewBuffer(vote))
	fmt.Println("CODE ", req2.Status)
	fmt.Println("-----------------------------------------------------------")
	fmt.Println()

	fmt.Println("-------------------------- VOTE ---------------------------")
	new_vote2 := client.VoteRequest{AgentID: "agt2", VoteID: "vote0", Prefs: []comsoc.Alternative{1, 3}}
	vote2, _ := json.Marshal(new_vote2)
	req3, _ := http.Post(url2, "application/json", bytes.NewBuffer(vote2))
	fmt.Println("CODE ", req3.Status)
	fmt.Println("-----------------------------------------------------------")
	fmt.Println()

	fmt.Println("-------------------------- VOTE ---------------------------")
	new_vote3 := client.VoteRequest{AgentID: "agt3", VoteID: "vote0", Prefs: []comsoc.Alternative{3, 1, 1}}
	vote3, _ := json.Marshal(new_vote3)
	req4, _ := http.Post(url2, "application/json", bytes.NewBuffer(vote3))
	fmt.Println("CODE ", req4.Status)
	fmt.Println("-----------------------------------------------------------")
}

// addVoteAndAskResult is another example of how to use the client side of the server.
// We check if we can vote after the deadline is passed and if we can get the results of
// the created ballot (returned by the created ballot agent).
// If you want to test, don't forget to modifiy the deadline (same format as createBallotAndVote).
func addVoteAndAskResult(deadline time.Time) {
	if deadline.Sub(time.Now()) < 0 {
		fmt.Println("-------------------------- VOTE ---------------------------")
		url2 := "http://127.0.0.1:8080/vote"
		new_vote4 := client.VoteRequest{AgentID: "agt4", VoteID: "vote0", Prefs: []comsoc.Alternative{3, 1, 2}}
		vote4, _ := json.Marshal(new_vote4)
		req5, _ := http.Post(url2, "application/json", bytes.NewBuffer(vote4))
		fmt.Println("CODE ", req5.Status)
		fmt.Println("-----------------------------------------------------------")
		fmt.Println()

		fmt.Println("------------------------- RESULTS -------------------------")
		result := client.ResultRequest{BallotID: "vote0"}
		res_data, _ := json.Marshal(result)
		url3 := "http://127.0.0.1:8080/result"
		resultat, _ := http.Post(url3, "application/json", bytes.NewBuffer(res_data))
		buf2 := new(bytes.Buffer)
		buf2.ReadFrom(resultat.Body)
		resp2 := client.ResultReturned{}
		_ = json.Unmarshal(buf2.Bytes(), &resp2)
		fmt.Println()
		fmt.Println("CODE ", resultat.Status)
		if resultat.StatusCode == 200 {
			fmt.Println("RESULTS: ", resp2)
		}
		fmt.Println("-----------------------------------------------------------")
	}
}

func main() {
	deadline, _ := time.Parse("Jan 2, 2006 at 3:04pm", "Nov 7, 2022 at 11:07am")
	if deadline.Sub(time.Now()) > 0 {
		createBallotAndVote()
	} else {
		addVoteAndAskResult(deadline)
	}
}
