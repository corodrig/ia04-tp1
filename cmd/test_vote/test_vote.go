// Created by Côme Rodriguez and Luning Yang

package main

import (
	"fmt"
	"ia04/comsoc"
)

func main() {
	var (
		alt1, alt2, alt3 comsoc.Alternative = 1, 2, 3
		prof                                = make(comsoc.Profile, 4)
		pref1                               = [3]comsoc.Alternative{alt1, alt2, alt3}
		pref2                               = [3]comsoc.Alternative{alt3, alt1, alt2}
		pref3                               = [3]comsoc.Alternative{alt2, alt1, alt3}
		alts                                = [3]comsoc.Alternative{alt1, alt2, alt3}
		t1                                  = []comsoc.Alternative{4, 5, 1, 2, 3, 6}
		t2                                  = []comsoc.Alternative{6, 1, 5, 3, 4, 2}
		t3                                  = []comsoc.Alternative{1, 5, 6, 3, 2, 4}
		t4                                  = []comsoc.Alternative{5, 1, 3, 6, 2, 4}
		profile                             = make(comsoc.Profile, 4)
		aa                                  = []comsoc.Alternative{1, 2, 3, 4, 5, 6}
	)
	prof[0] = pref1[:]
	prof[1] = pref2[:]
	prof[2] = pref3[:]
	prof[3] = pref1[:]

	profile[0] = t2
	profile[1] = t3
	profile[2] = t4
	profile[3] = t1

	fmt.Println("--------------------- MAJORITY ---------------------")
	o_maj := []comsoc.Alternative{2, 1, 3}
	fmt.Println("ORDER ", o_maj)
	fmt.Println("PROFILE ", prof)
	tie_func_maj := comsoc.TieBreakFactory(o_maj[:])
	majswf := comsoc.SWFFactory(prof, alts[:], comsoc.MajoritySWF, tie_func_maj)
	c_maj, _ := majswf(prof)
	c_maj2, _ := comsoc.MajoritySWF(prof, alts[:])
	fmt.Println("MAJORITY SWF ", c_maj)
	fmt.Println("MAJORITY SWF CHECK ", c_maj2)
	fmt.Println("----------------------------------------------------")
	fmt.Println()
	fmt.Println()
	fmt.Println("----------------------- BORDA ----------------------")
	o_bor := []comsoc.Alternative{2, 3, 1}
	fmt.Println("ORDER ", o_bor)
	fmt.Println("PROFILE ", prof)
	tie_func_bor := comsoc.TieBreakFactory(o_bor[:])
	borswf := comsoc.SWFFactory(prof, alts[:], comsoc.BordaSWF, tie_func_bor)
	c_bor, _ := borswf(prof)
	c_bor2, _ := comsoc.BordaSWF(prof, alts[:])
	fmt.Println("BORDA SWF ", c_bor)
	fmt.Println("BORDA SWF CHECK ", c_bor2)
	fmt.Println("----------------------------------------------------")
	fmt.Println()
	fmt.Println()
	fmt.Println("--------------------- APPROVAL ---------------------")
	t := [6]int{2, 1, 3, 1, 1, 2}
	o_app := []comsoc.Alternative{3, 2, 1}
	fmt.Println("ORDER ", o_app)
	fmt.Println("PROFILE ", prof)
	tie_func_app := comsoc.TieBreakFactory(o_app[:])
	app_count, _ := comsoc.ApprovalSWF(prof, t[:], alts[:])
	c_app := comsoc.TransformCountToSlice(app_count, tie_func_app)
	fmt.Println("APPROVAL SWF ", c_app)
	fmt.Println("APPROVAL SWF CHECK ", app_count)
	fmt.Println("----------------------------------------------------")
	fmt.Println()
	fmt.Println()
	fmt.Println("--------------------- COPELAND ---------------------")
	o_cop := []comsoc.Alternative{2, 5, 1, 3, 6, 4}
	fmt.Println("ORDER ", o_cop)
	fmt.Println("PROFILE ", profile)
	tie_func_cop := comsoc.TieBreakFactory(o_cop[:])
	cswf := comsoc.SWFFactory(profile, aa[:], comsoc.CopelandSWF, tie_func_cop)
	c, _ := cswf(profile)
	c2, _ := comsoc.CopelandSWF(profile, aa[:])
	fmt.Println("COPELAND SWF ", c)
	fmt.Println("COPELAND SWF CHECK ", c2)
	fmt.Println("----------------------------------------------------")
	fmt.Println()
	fmt.Println()
	fmt.Println("----------------------- STV ------------------------")
	o_stv := []comsoc.Alternative{4, 1, 5, 2, 6, 3}
	fmt.Println("ORDER ", o_stv)
	fmt.Println("PROFILE ", profile)
	tie_func_stv := comsoc.TieBreakFactory(o_stv[:])
	stv_count, _ := comsoc.STV_SWF(profile, aa[:], aa)
	c_stv := comsoc.TransformCountToSlice(stv_count, tie_func_stv)
	fmt.Println("STV SWF ", c_stv)
	fmt.Println("STV SWF CHECK ", stv_count)
	fmt.Println("----------------------------------------------------")
	fmt.Println()
	fmt.Println()
	fmt.Println("---------------------- KEMENY ----------------------")
	fmt.Println("BEST PERMUTATION ", comsoc.KemenySWF(profile, aa[:]))
	fmt.Println("BEST ALTERNATIVE ", comsoc.KemenySCF(profile, aa[:]))
	fmt.Println()
	fmt.Println()
}
