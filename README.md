# Ia04 Tp1 - Côme Rodriguez & Luning Yang



## Getting started

This repository contains our work for the TP. We created a vote server where a client can create a vote place (named *ballot*) with registered voters (named *voter*), add a vote for a specific voter in a specific vote place and ask the results of a vote in a specific vote place.  
  
To install the repository, you can run the following in a terminal:
```
go install gitlab.utc.fr/corodrig/ia04-tp1@latest
```

## Organisation of the server

The server is named **Manager Agent**. It's responsible of all the vote places (*ballots*) the client has created. It stores all the vote places and distributes the IDs to each new vote place created.  
  
A vote place is named a **Ballot Agent**. When creating a new vote place in the Manager Agent, we attribute some voters to this vote place. The **Ballot Agent** is then responsible of creating new **Voter Agents** (see below). A **Ballot Agent** is represented by a voting rule, a deadline (after this deadline no votes will be accepted), a list of voters and a number of alternative. When the deadline is passed, the client can ask the results of the vote in a **Ballot Agent**  
  
A voter is a **Voter Agent**. It expresses ots preferences between the alternatives in a vote place. A **Voter Agent** can vote on a specific vote place where it is registered

## List of available voting rules

A client can create a vote place in the Manager Agent with the following voting rules:
- Approval
- Borda
- Copeland
- Kemeny
- Majority
- Simple Transferable Vote (STV)

## How to use the Manager Agent

### Start the Manager Agent

To start the Manager Agent, the client can run the following:
```golang
package main

import ma "ia04/cmd/launch_manager"

func main () {
    ma.LaunchManagerAgent(":8080")
}
```
where the argument of LaunchManagerAgent is the port where the client want to start the Manager Agent.

### Create a new vote place in the Manager Agent

After starting the Manager Agent, a client can create a new vote place with the following peace of code:
```golang
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	client "ia04/agt/client"
	"net/http"
)

new_bal := client.NewBallotRequest{
    Rule: "majority",
    Deadline: "Nov 7, 2022 at 0:23am",
    Voters_ids: []string{"agt1", "agt2", "agt3", "agt4"},
    Alts: 3
}
data, _ := json.Marshal(new_bal)
url := "http://127.0.0.1:port/new_ballot"
req, _ := http.Post(url, "application/json", bytes.NewBuffer(data))
buf := new(bytes.Buffer)
buf.ReadFrom(req.Body)
resp := client.NewBallotReturned{}
_ = json.Unmarshal(buf.Bytes(), &resp)
fmt.Println("CODE ", req.Status)
fmt.Println("RESP ", resp)
```
replacing *port* with the port given as argument in LaunchManagerAgent. The *Rule* attribute has to be choosen between the ones listed on **List of available voting rules**. The *Deadline* attribute has to have the following format: "Jan 2, 2009 at 3:04pm" (be careful, the hour uses UTC and not UTC+1, sorry french guys, but you will to retrieve one hour). The *Voters_ids* attribute contains all the IDs of the voters registered in this vote place. The **Ballot Agent** created is responsible of creating all the corresponding **Voter Agents**.  
  
The request will be sent to the Manager Agent. The response of the request can be:
- 201 Created (the new **Ballot Agent** created will return its ID)
- 400 Bad Request (if the request is not good, or if the deadline is not in a good format)
- 501 Not Implemented (if the voting rule is not one listed above)

### Create a new vote for a voter in a vote place

To create a new vote for a specific voter in a specific vote place, the client can use the following code:
```golang
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	client "ia04/agt/client"
    "ia04/comsoc"
	"net/http"
)

new_vote := client.VoteRequest{AgentID: "agt1", VoteID: "vote0", Prefs: []comsoc.Alternative{1, 2, 3}}
	vote, _ := json.Marshal(new_vote)
	url2 := "http://127.0.0.1:port/vote"
	req2, _ := http.Post(url2, "application/json", bytes.NewBuffer(vote))
	fmt.Println("CODE ", req2.Status)
```
replacing *port* with the port given as argument in LaunchManagerAgent. The needed arguments are the *AgentID*, the *VoteID* and the preferences of the voter. Options can be given in an optional argument *Options*.  
  
The result of the request can be:
- 200 OK
- 400 Bad request (if there is a problem in the request)
- 403 Forbidden (if the voter has already voted for this vote place)
- 501 Not implemented (if the vote place does not exist and/or if the voter is not registered in this vote place)
- 503 Unavailable (if the deadline is passed)

### Get results of a vote in a vote place

To get the result of a vote in a vote place, the client can use the following code:
```golang
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	client "ia04/agt/client"
	"net/http"
)

result := client.ResultRequest{BallotID: "vote0"}
res_data, _ := json.Marshal(result)
url3 := "http://127.0.0.1:port/result"
resultat, _ := http.Post(url3, "application/json", bytes.NewBuffer(res_data))
buf2 := new(bytes.Buffer)
buf2.ReadFrom(resultat.Body)
resp2 := client.ResultReturned{}
_ = json.Unmarshal(buf2.Bytes(), &resp2)
fmt.Println()
fmt.Println("CODE ", resultat.Status)
if resultat.StatusCode == 200 {
    fmt.Println("RESULTS: ", resp2)
}
```
replacing *port* with the port given as argument in LaunchManagerAgent. The only argument needed in this request is the ID of the vote place.
The result of this request can be:
- 200 OK (the **Ballot Agent** will return the winner of the vote, result of a the social choice function, and the ranking of the vote, result of the social welfare function)
- 400 Bad request (if there is a problem in the request)
- 425 Too early (if the deadline is not already passed)
